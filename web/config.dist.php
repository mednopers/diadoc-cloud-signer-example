<?php

return [
    'CLOUD_SIGNER_URL' => 'http://127.0.0.1',
    'CLOUD_SIGNER_TOKEN' => 'secret',
    'DEVELOPER_KEY' => 'key',
    'CERTIFICATE_FILE' => '/certificates/certificate.pem'
];