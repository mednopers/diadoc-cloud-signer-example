<?php

use AgentSIB\Diadoc\DiadocApi;
use AgentSIB\Diadoc\Model\CloudOpensslSignerProvider;

require __DIR__.'/../vendor/autoload.php';
$config = require 'config.php';

try {
    $signerProvider = new CloudOpensslSignerProvider(
        $config['CLOUD_SIGNER_URL'],
        $config['CLOUD_SIGNER_TOKEN']
    );

    $client = new DiadocApi($config['DEVELOPER_KEY'], $signerProvider);
    $client->authenticateCertificate(dirname(__DIR__, 1) . $config['CERTIFICATE_FILE']);

    $signed = $client->generateSignedContentFromFile('../README.md');

    if (!$signed->hasContent()) {
        throw new \RuntimeException('No signed content');
    }

    echo sprintf('<p>Signature: %s</p>', base64_encode($signed->getSignature()->getContents()));
} catch (Exception $e) {
    print_r($e->getMessage());
    print_r($e->getTraceAsString());
}
