# Cloud Signer Example

Пример использования [diadoc-cloud-signer](https://bitbucket.org/mednopers/diadoc-cloud-signer/src/45020c92f449b836164b6fe1738e1b686aba03f6/?at=feature%2FCS-001)

Вывести список доступных команд:
```bash
$ make
```

## Установка

Инициализировать окружение:
```bash
$ make env-init
```
Отредактировать параметры в файле `.env`.

Для установки запустить: 
```bash
$ make install
```