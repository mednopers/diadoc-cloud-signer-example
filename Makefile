.DEFAULT_GOAL := help
.PHONY : help
help : Makefile
	@sed -n 's/^##//p' $<

##
## Установка
##-------------------------------------------------------------
.PHONY: install env-init composer-install

## install		: Создать окружение
install: docker-build composer-install

## env-init		: Инициализация окружения
env-init:
	rm -f .env
	cp .env.dist .env

## composer-install	: Установка библиотек
composer-install:
	docker-compose exec php-fpm composer install

##
## Docker
##-------------------------------------------------------------
.PHONY: up start stop remove

## up			: Запустить контейнеры
up: 
	docker-compose up -d

## stop			: Остановить контейнеры
stop:
	docker-compose stop

## remove			: Удалить контейнеры
remove:
	docker-compose down --remove-orphans

## docker-build		: Сборка и запуск контейнеров
docker-build:
	docker-compose up --build -d

##
## Utils
##-------------------------------------------------------------
.PHONY:  bash root-bash

## bash			: Вход в контейнер под учеткой www-data
bash:
	docker-compose exec php-fpm bash

## root-bash		: Вход в контейнер под учеткой root
root-bash:
	docker-compose exec --user=root php-fpm bash
